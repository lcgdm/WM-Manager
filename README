=============================
==== WORKER NODE MANAGER ====
=============================

0. REQUIREMENTS
The program was tested on Python 2.7.3 and 2.6.x. There are no (known) dependencies.


1. GENERAL IDEA
This script manages the pool of virtual machines that bamboo (or, in fact, any other application) uses. 
Basically it detects machines that are powered off and attempts to rebuild them (which also causes them to start up). If there are 
too many instances comparing to the number configurated, the machine will be deleted.
On the other hand, if there aren't enough instances compared to the number configured, it will create new instances. As service 
accounts can't instantiate virtual machines, this feature only works if the account configured is not a service account.
Optionally, a maximum running time can be specified. If an instance is running for longer than this time, the instance is rebuild 
(or deleted) as well, whatever the state it is in.


2. USAGE
usage: WNManager.py [-h] [--version] [-v] [-q] [-c configFile] [-d] [-f]
                    [-l logFile] [-e EMAIL]

Openstack pool balancer.

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -v, --verbose         debug messages enabled
  -q, --quiet           no output to stdout and stderr. Log file is still
                        generated.
  -c configFile, --config configFile
                        configuration file to be used. If not specified, it
                        will try WNManager.conf.xml. Otherwise, default values
                        are used.
  -d, --dryrun          Run without doing any action, just simulating
  -f, --force-recycle   Force recycling of all machines, either by rebuilding
                        or deleting them
  -l logFile, --log logFile
                        Location of the log file
  -e EMAIL, --email EMAIL
                        On error, send email to the address provided


---Some additional notes about the parameters---

configFile: Specify configuration file to be loaded by the program. This is needed for the program to be useful at all. See point 3.
force-recycle: Will either rebuild or delete all the instances found. Which action to be taken depends on the number of current 
instances versus number of configured instances.
logFile: Location of the logFile. A rotation mechanism is automatically garanteed.
verbose: Enable debug messages, both to stdout and to log file.
email: On error, an email with the error description will be sent to the address provided, using smtp.



3. CONFIGURATION FILE
A configuration file, written in XML, specifies the behavior of the script. 
The connection section specifies how to access the OpenStack webservice:

	<connection>
		<username>username</username>
		<url>https://ibex-cloud-controller.cern.ch:5000/v2.0</url>
		<tenant>Agile CI</tenant>
		<service>False</service>
	</connection>
	
The 'service' indicates if the username is a normal account or a service account. Service accounts cannot create new instances, so
this feature is disabled if the username is a service account, and only the rebuild of existing instances will be done.

---

You can configure as many images as you want, as following:
	<images>
		<image>
			<name>slc6-server-x86_64</name>
			<basename>test-bamboo-sl6</basename>
			<flavor>m1.small</flavor>
			<count>5</count>
			<maxTime>86400</maxTime>
			<keypair>dmeneses-keypair</keypair>
			<script>/home/dmeneses/workspace/WNManager/src/patch.sh</script>
			<sshIdentity>/home/dmeneses/workspace/WNManager/src/key</sshIdentity>
		</image>
	</images>

Some notes:
-name: name of the image from which the rebuild is done. Must be the name of a valid image on the image library in OpenStack.
- basename: The instances will be detected for rebuild/deletion and created based on this name. For example, with the given example, 
it would detect the instances with the names 'test-bamboo-sl6-01', 'test-bamboo-sl6-05', etc...	
-count: How many instances from the image should exist. At a given time, according to this number and the number of existing 
instances of the image and wheter the username being used is a service account or not, the script may create new instances or 
delete existing instances that are shutoff or expired (instead of rebuilding them), so to have the specified number of instances.
-maxTime: defines maximum time an instance can be running. After this time passed, the image is rebuild or deleted, whatever the state it is in.


4. EXAMPLES OF USAGE
python WNManager.py -v -c WNManager.conf.xml -email my.email@example.org -l /tmp/wnmanager.log

It is usually convenient to configure it as a cronjob. It program uses a lock file, so it is safe to run it in short periods, 
such as every minute.



5. COMMENTS / SUPPORT
cristovao.cordeiro@cern.ch

