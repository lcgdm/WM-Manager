'''
Created on Apr 29, 2013

@author: dmeneses
'''
from novaclient.v1_1 import client
from novaclient import exceptions
import xml.etree.ElementTree as ET
from datetime import datetime
import sys
import signal
import traceback
import os
import logging.handlers
import argparse
import socket
from time import sleep

VERSION='1.4'
LOCK_FILE='/tmp/.wnmanager.lock'
WAIT=10

conn=None
logger=None
configFile=None
dryRun=None
forceRecycle=None
password=None

#Objects to change/fill from configuration file
connection_conf = {
    'url' : 'https://ibex-cloud-controller.cern.ch:5000/v2.0',
    'tenant' : 'Agile CI',
    'username' : 'user',
}

image_conf = []

def signal_handler(signal, frame):
        print 'catched SIGINT, exiting.'
        sys.exit(1)

def init():
    '''Parses arguments and sets up logging
    '''
    global logger
    global configFile
    global dryRun
    global forceRecycle
    global password
    
    #Catch CTRL+C (SIGINT)
    signal.signal(signal.SIGINT, signal_handler)

    #Parse arguments
    parser = argparse.ArgumentParser(description='Openstack pool balancer.')
    parser.add_argument('--version', action='version', version='%(prog)s ' + VERSION)
    parser.add_argument('-v', '--verbose', action='store_true', help='debug messages enabled')
    parser.add_argument('-q', '--quiet', action='store_true', \
                        help='no output to stdout and stderr. Log file is still generated.')
    parser.add_argument('-c', '--config',  action='store', default='WNManager.conf.xml', \
                        metavar='configFile', dest='configFile', \
        help='configuration file to be used. If not specified, it will try %(default)s. Otherwise, default values are used.')
    parser.add_argument('-d', "--dryrun", action='store_true', dest='dryRun', \
                        help='Run without doing any action, just simulating')
    parser.add_argument('-f', "--force-recycle", action='store_true', dest='forceRecycle', \
                        help='Force recycling of all machines, either by rebuilding or deleting them')
    parser.add_argument('-l', "--log", action='store', default='/tmp/wnmanager.log', metavar='logFile', \
                        help='Location of the log file')
    parser.add_argument('-e', "--email", action='store', default=None, help='On error, send email to the address provided')
    parser.add_argument('-p', "--password", action='store', default=None, \
    	help="Password to be used in openstack. If not defined, it will try to read 'OS_PASSWORD' environment variable")
    ns = parser.parse_args()
    
    #get configuration file
    configFile = ns.configFile
    dryRun = ns.dryRun    
    forceRecycle = ns.forceRecycle    
    if ns.verbose:
        lvl = logging.DEBUG
    else:
        lvl = logging.INFO
        
    logger = logging.getLogger(__name__)
    
    #set file logging
    fmt = logging.Formatter('%(asctime)s:%(process)d:%(levelname)s:%(message)s')
    fileh = logging.handlers.RotatingFileHandler(ns.log, maxBytes=50000000, backupCount=10)
    fileh.setFormatter(fmt)
    
    #set console logging
    console = logging.StreamHandler(sys.stdout)
    if ns.quiet:
        console.setFormatter(logging.NullHandler)
    else:
        console.setFormatter(fmt)
    
    root = logging.getLogger()
    root.setLevel(logging.ERROR)
    root.addHandler(fileh)
    root.addHandler(console)
    
    logger.setLevel(lvl)
    logger.addHandler(fileh)
    logger.addHandler(console)
    logger.propagate = False

    #Email on error
    if ns.email is not None:
        frm = os.getlogin() + '@' + socket.gethostname()
        sbj = 'Error in bamboo agent manager'
        smpth = logging.handlers.SMTPHandler('localhost', frm, [ns.email], sbj)
        smpth.setLevel(logging.ERROR)
        root.addHandler(smpth)
        
    if ns.verbose:
        pass
    if ns.quiet:
        pass
    if ns.dryRun:
        logger.debug('Dry run mode ON')
    
    #Password
    if ns.password is not None:
        password = ns.password
    elif os.environ.get('OS_PASSWORD') is not None:
        password = os.environ.get('OS_PASSWORD')
    else:
        logger.error('No password defined!')
        sys.exit(0)
    
    return

def total_seconds(td):
    '''For compatibility with Python 2.6. Same as 2.7's timedelta.total_seconds()
    '''
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6
    
def read_settings(f):
    '''Loads configuration
    '''
    global image_conf
    global connection_conf
    
    tree = None
    
    try:
        tree = ET.parse(f)
    except IOError:
        logger.error("Couldn\'t load configuration file: " + f)
        sys.exit(1)
    
    logger.debug("Reading settings from '" + f + "'")
        
    root = tree.getroot()
    
    if root.tag != 'BalancerConfiguration':
        root = root.find('.//BalancerConfiguration')  
        if root is None:
            logger.error('invalid configuration file!')
            logger.info('Using default settings')
            return
    
    #Connection configuration
    node_conn = root.find('connection')
    if node_conn is not None:
        for prop in node_conn:
            connection_conf[prop.tag] = prop.text
    
    #Servers configuration
    node_image = root.find('images')
    if node_image is not None:
        image_conf = []
        
        node_images = node_image.findall('image')
        
        logger.debug('Found configuration for ' + str(len(node_images)) + ' image(s):')
        
        if node_images is not None:
            for i in node_images:
                #defaults for optional stuff
                image = {'maxTime':259200,'count':1,'flavor':'m1.medium'}
                
                for prop in i:
                    image[prop.tag] = prop.text
  
                if (not 'name' in image) or (not 'basename' in image):
                    logger.error("Found invalid image: needs to have at least 'name' and 'instName'. Skipping.")
                    continue
                
                try:
                    int(image['count'])
                    int(image['maxTime'])
                except (KeyboardInterrupt, SystemExit):
                    raise
                except:
                    logger.error('Found invalid image: count and maxTime must be integers. Skipping.')
                    continue
                
                image_conf.append(image)
                logger.debug(image)
    return

'''
Will raise:
novaclient.exceptions.NotFound: No Image matching {'human_id': 'slc6s-server-x86_64'}. (HTTP 404) if image is invalid
novaclient.exceptions.NotFound: No Flavor matching {'name': 'm1s.small'}. (HTTP 404) if flavor is invalid
novaclient.exceptions.BadRequest: Hostname already in use (HTTP 400) (Request-ID: req-27c2e621-1ad1-4672-b4b2-2b8a27799d4f)
'''
def create_instances(inst_names, basename, image_name, flavor, keyname, script, identity, count=1):
    #Check if we have a connection to WS and if args are valid
    if conn is None:
        raise Exception('No connection to the WS!')  
    if count <= 0:
        raise Exception('invalid count')
    
    #Check if image_name/flavor exist and get corresponding objects
    image_obj = conn.images.find(human_id=image_name)
    flavor_obj = conn.flavors.find(name=flavor)
        
    #create new instances
    while count > 0:
        name = find_name(basename, inst_names)
        inst_names.append(name)
        logger.info('Creating: ' + name)
        try:
            if not dryRun:
                conn.servers.create(name, image_obj, flavor_obj, { 'automanaged' : 'true' }, \
                            userdata = script.strip().replace('\t',''), key_name=keyname)
        except exceptions.BadRequest as e:
            if "Hostname already in" in e.message:
                continue
            else:
                raise
        count -= 1
        sleep(WAIT)

def find_name(basename, inst_names):      
    for i in range(1,99):      
        number = str(i).zfill(2)
        name = basename + '-' + number

        if name in inst_names:
            continue
        
        return name
      
def find_instances(basename):
    '''Find all instances with specific basename. For example, giving basename 'test-sl6' could return instances
    with the names 'test-sl-01' and test-slasd'
    
    Args:
        basename: String with which the instance name starts with
    '''
    inst_objs = []
    inst_names = []
    inst_list = conn.servers.list()
    
    for inst in inst_list:
        if inst.name.startswith(basename) and len(inst.name) == (len(basename) +3):
            inst_objs.append(inst)
            inst_names.append(inst.name)
    
    return inst_objs,inst_names

def manage_nodes():
    #Go through all images
    logger.debug(str(len(image_conf)) + ' image(s) to check..')
    
    for image in image_conf:
        logger.info('Checking %s' % image['name'])
        try:
            image_obj = conn.images.find(human_id=image['name'])
        except (KeyboardInterrupt, SystemExit):
            raise
        except exceptions.NotFound:
            logger.error('Invalid image (not found): %s. Skipping.' % image['name'])
            continue
        except:
            logger.exception('Error searching image %s. Skipping.' % image['name'])
            continue
        
        #Get instances from this image
        try:
            inst_objs, inst_names = find_instances(image['basename'])
            n = len(inst_objs)
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            logger.exception('Error looking for instances for %s' % image['basename'])
            continue
                
        logger.debug('Found %d instances, should have %d' % (n, int(image['count'])))
            
        #Go through all instances, delete if needed
        for o in inst_objs:
            delta = datetime.utcnow() - datetime.strptime(o.updated, '%Y-%m-%dT%H:%M:%SZ')
            logger.debug('Checking %s (%s %s)' % (o.name, o.status, str(delta)))
            
            #Handle machines
            if (o.status == 'SHUTOFF' or o.status == 'ERROR' \
                        or total_seconds(delta) > int(image['maxTime']) or forceRecycle) and o.name != 'squid-repo-cache':
                if(o.status != 'SHUTOFF' and total_seconds(delta) > int(image['maxTime'])):
                    logger.warn('Recycling machine %s running over maxTime' % o.name)
                elif o.status != 'SHUTOFF' and forceRecycle:
                    logger.warn('Forcing recycling of %s' % o.name)
                
                logger.debug('Deleting %s' % o.name)
                
                if not dryRun:
                    try:
                        o.delete()
                        # CRIS: comment n -= 1 so that the termination numbers don't get higher then the count - for certificate purposes
                        #n -= 1
                        sleep(WAIT)
                    except (KeyboardInterrupt, SystemExit):
                        raise
                    except:
                        logger.exception('Error deleting')
                        continue
            elif o.status == 'DELETED':
                # CRIS: comment n -= 1 so that the termination numbers don't get higher then the count - for certificate purposes
                #n -= 1
                o.delete()
                
        #Create more if necessary
        if n < int(image['count']):
            try:
                logger.debug('Creating %d instances of %s %s' % \
                              (int(image['count']) - n, image['name'], image['flavor']))
                create_instances(inst_names, image['basename'], image['name'], image['flavor'], \
                                 image['keypair'], image.get('script'), image.get('sshIdentity'), int(image['count']) - n)
            except (KeyboardInterrupt, SystemExit):
                raise
            except Exception:
                logger.exception('Error creating instances for %s' % image['name'])
  
    return

def checkPID():
    '''Manages single instance of the program through a lock file. It will exit if lock file exists with a valid PID.
    '''
    try:
        pidfile = open(LOCK_FILE, "r")
        pidfile.seek(0)
        old_pid = pidfile.readline()
           
        if os.path.exists("/proc/%s" % old_pid):
            logger.info("Instance of the program is running with pid %s. Exiting." % old_pid)
            sys.exit()
        else:
            logger.warn("Lock file present but the program is not running. Removing lock file.")
            os.remove(LOCK_FILE)
    except (KeyboardInterrupt, SystemExit):
        raise
    except IOError:
        pass  
      
    pidfile = open(LOCK_FILE, "w")
    pidfile.write("%s" % os.getpid())
    pidfile.close()
    
if __name__ == '__main__':
    try:
        beginTime = datetime.now()
        init()
        checkPID()
        read_settings(configFile)
        print image_conf
        print image_conf[0]['script'].strip().replace('\t','')
        #sys.exit()
        #Connect to Nova WS
        logger.info('Connection parameters: %s' % (connection_conf))
    
        #Some problems getting system-specific certificate bundle: http://bugs.python.org/issue13655 so we disable it
        conn = client.Client(connection_conf['username'], password, connection_conf['tenant'], \
                             connection_conf['url'], service_type="compute", insecure=True)
        
        manage_nodes()

    except (KeyboardInterrupt, SystemExit):
        logging.shutdown()
        sys.exit(0)
    except:
        if logger is not None:
            logger.exception("error")
        print 'error: ', traceback.format_exc()
        
    #Done!
    if os.path.isfile(LOCK_FILE):
        os.remove(LOCK_FILE)

    logger.info('Done - took ' + str(datetime.now() - beginTime))
    logging.shutdown()
    
    
