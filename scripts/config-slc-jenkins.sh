#!/bin/sh
packages="wget subversion rpm-build mock CERN-CA-certs kernel-module-openafs-`uname -r` openafs-client openafs-krb5 krb5-workstation java-1.7.0-openjdk java-1.7.0-openjdk-devel"
password="Bigadi92"
cert_dir="/afs/cern.ch/user/b/bamboovm/private/hostcerts"
hostname=`hostname -f`
bamboo_cert_dir="/root/sdcint/BambooServerCert/Bamboo_server_cert.crt"
#jenkins_id_rsa_pub='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3UWGwa51S5M7wxveXCGtFZcAyNCH6N+dLad3LtoqHMTk8Dn2ViJcC6yOy1GAC+8JY5PIEO5h10yS7DW0K/EhYs/nC1RxhDp5VRpUPhgcSOV4DAfxV4K1PfOzv0IqUSVsAFHOuWOUj5UhJyj4EfTfZXBc1zFJmJZv48IhViCb0G4jX/ptPaB5oyKFVEvUbshLFxZ0IhCRVxdZUnOa8IDIunHHW52273ZG829W3Ya5NZIQvCs3/9UevZeynp0FvhNvaXBpMk4Ac69emiP04IPC5O5efHtCtIy0/ch0UL8jN/9JV+cvjvadN1gOuJyZQax89CD8AEehjwXqf/YS8Uebp jenkins@jenkins-master-498690dd1e.cern.ch'
jenkins_id_rsa_pub='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3UWGwa51S5M7wxveXCGtFZcAyNCH6N+dLad3LtoqHMTk8Dn2ViJcC6yOy1GAC+8JY5PIEO5h10yS7DW0K/EhYs/nC1RxhDp5VRpUPhgcSOV4DAfxV4K1PfOzv0IqUSVsAFHOuWOUj5UhJyj4EfTfZXBc1zFJmJZv48IhViCb0G4jX/ptPaB5oyKFVEvUbshLFxZ0IhCRVxdZUnOa8IDIunHHW52273ZG829W3Ya5NZIQvCs3/9UevZeynp0FvhNvaXBpMk4Ac69emiP04IPC5O5efHtCtIy0/ch0UL8jN/9JV+cvjvadN1gOuJyZQax89CD8AEehjwXqf/YS8Uebp jenkins-master'
bamboo_pubkey='ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAIEAxG/oiavLOogk/KJv8a5tWSXiAMBy1JiRdtBnikRswq1kZMI19QCnUeemLMZ3VcIFD7xubqvbmypssHFCtTRAxyL5NKszFj5b+VPOS7M+G7itRUZYDJh/P6ALCwYMmAJ9c38EEidH86u81CprUfj/9gMiDJwVuHqcj+bO7/JJOh8='
newjenkins_id_rsa_pub='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC94nCEA2yqtSOS/nTdzMlYl6y5Wx60q3vW5cPsobYV4dwEaFM7CugQl4ur6f/e0u4CET5oGQjd9EJwAD1JOy+HqYJzw8GOiUK7IXXTyT7bI0uhlIjHWJOPq90bz2ky03gpem2jE5/Y2pp0OP+UQiwIELKp55vII5nAC7m8qsE6TBxTiNh0WakzNx15Yw+EXJOPlOa8M8F+gstREgVE/lr+BqYtCmtRqNHcmoHMrSQuc4uNW1fQm7VVSzGPrPayom2KlcZ0pYaiyfbXXAygy5XldRiADS3999BN37foJiwN1Qs2MwHFSKspUwvzb9v3RLfWCOMKSA5t+O8yPL9RLitr jenkins-newmaster'
export PATH=$PATH:/usr/bin:/usr/kerberos/bin/
cd /root
setenforce Permissive
#Install packages
yum -y -q install $packages
if [ $? -ne 0 ]; then
  echo "Failed to install packages" && exit 1
fi

#configure AFS/kerberos
rm -rf /etc/krb5.conf
wget -nv http://linux.web.cern.ch/linux/docs/krb5.conf -O /etc/krb5.conf
if [ $? -ne 0 ]; then
  echo "Failed to get afs/kerberos config file" && exit 1
fi

mkdir -p /usr/vice/etc/
echo "cern.ch" > /usr/vice/etc/ThisCell
service afs start

#get host certificate/key

echo ${password} | kinit bamboovm
if [ $? -ne 0 ]; then
  echo "Failed to get kerberos credentials" && exit 1
fi

aklog
if [ $? -ne 0 ]; then
 echo "Could not use kerberos token to get an afs token for writing to the repository" && exit 1
fi

mkdir -p /etc/grid-security/
cp "${cert_dir}/hostkey.pem.${hostname}" "/etc/grid-security/hostkey.pem"; ret1=$?
cp "${cert_dir}/hostcert.pem.${hostname}" "/etc/grid-security/hostcert.pem"; ret2=$?
cp "${cert_dir}/hostcert.pkcs12.${hostname}" "/etc/grid-security/hoscert.pkcs12"; ret3=$?
kdestroy

if [ $ret1 -ne 0 -o $ret2 -ne 0 -o $ret3 -ne 0 ]; then
  echo "Failed to retrieve at least one of the host certificate files" && exit 1
fi

#put local address translation in hosts file
ipaddress=`/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'`
echo "${ipaddress} `hostname -f` `hostname`" >> /etc/host

#configure jenkins ssh key
echo ${newjenkins_id_rsa_pub} >> /root/.ssh/authorized_keys
echo ${jenkins_id_rsa_pub} >> /root/.ssh/authorized_keys
echo ${bamboo_pubkey} >> /root/.ssh/authorized_keys

exit 0
