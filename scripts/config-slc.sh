#!/bin/sh

packages="java-1.6.0-openjdk java-1.6.0-openjdk-devel subversion rpm-build mock CERN-CA-certs kernel-module-openafs-`uname -r` openafs-client openafs-krb5 krb5-workstation"
bamboo_host="bamboo.its.cern.ch"
bamboo_agent="atlassian-bamboo-agent-installer-5.2.jar"
bamboo_home="/root/bamboo"
wrapper_conf="${bamboo_home}/conf/wrapper.conf"
password="Dilu-Wape++"
keystore_file="/root/keystore"
wait_time=10
shutdown_time=5
cert_dir="/afs/cern.ch/user/b/bamboovm/private/hostcerts"
hostname=`hostname -f`
bamboo_cert_dir="/root/sdcint/BambooServerCert/Bamboo_server_cert.crt"


export PATH=$PATH:/usr/bin:/usr/kerberos/bin/

########################################################################
function log() {
        echo ${1}
        logger -p user.info -t agentconfig ${1}
}

function logerror() {
        echo ${1} 1>&2;
        logger -p user.err -t agentconfig "ERROR ${1}"
}

function exit_error() {
  local m=$1
  logerror "${m}. Shutdown in ${shutdown_time} minutes."
  shutdown -h +${shutdown_time} &
  exit 1
}

#$1=file, $2=property, $3=new_value
function set_file_prop()
{
        grep "^${2}" $1 > /dev/null
        if [ $? -eq 0 ]; then
                #found line - modify it
                sed -i "s|^\(${2}=\).*$|\1${3}|" ${1}
        else
                #line not found - add it
                echo "${2}=${3}" >> ${1}
        fi

        return 0
}

########################################################################
#First prepare logging
yum -y install rsyslog

cat > /etc/rsyslog.d/30-agentconfig.conf << EOF
:syslogtag, isequal, "agentconfig:" @137.138.102.220
EOF

cat /etc/rsyslog.conf | grep "\$IncludeConfig" &> /dev/null
if [ $? -ne 0 ]; then
        echo "\$IncludeConfig /etc/rsyslog.d/*.conf" >> /etc/rsyslog.conf
fi

service syslog stop
service rsyslog restart

########################################################################
log "Starting configuration"

cd /root
setenforce Permissive
#Install packages
yum -y -q install $packages
if [ $? -ne 0 ]; then
  exit_error "Failed to install packages"
fi

#configure AFS/kerberos
rm -rf /etc/krb5.conf
wget -nv http://linux.web.cern.ch/linux/docs/krb5.conf -O /etc/krb5.conf
if [ $? -ne 0 ]; then
  exit_error "Failed to get afs/kerberos config file"
fi

mkdir -p /usr/vice/etc/
echo "cern.ch" > /usr/vice/etc/ThisCell
service afs start

#######################################################################
#install maven 3

wget http://mirror.switch.ch/mirror/apache/dist/maven/maven-3/3.1.1/binaries/apache-maven-3.1.1-bin.tar.gz
tar xf apache-maven-3.1.1-bin.tar.gz -C /usr/local/
export M2_HOME=/usr/local/apache-maven/apache-maven-3.1.1. 
export M2=$M2_HOME/bin
export PATH=$M2:$PATH

#######################################################################
#get host certificate/key

echo ${password} | kinit bamboovm
if [ $? -ne 0 ]; then
  exit_error "Failed to get kerberos credentials"
fi

aklog
if [ $? -ne 0 ]; then
 exit_error "Could not use kerberos token to get an afs token for writing to the repository"
fi

mkdir -p /etc/grid-security/
cp "${cert_dir}/hostkey.pem.${hostname}" "/etc/grid-security/hostkey.pem"; ret1=$?
cp "${cert_dir}/hostcert.pem.${hostname}" "/etc/grid-security/hostcert.pem"; ret2=$?
cp "${cert_dir}/hostcert.pkcs12.${hostname}" "/etc/grid-security/hoscert.pkcs12"; ret3=$?
kdestroy

if [ $ret1 -ne 0 -o $ret2 -ne 0 -o $ret3 -ne 0 ]; then
  exit_error "Failed to retrieve at least one of the host certificate files"
fi

#put local address translation in hosts file
ipaddress=`/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'`
echo "${ipaddress} `hostname -f` `hostname`" >> /etc/host

########################################################################
#Get bamboo client

wget -nv --no-check-certificate https://$bamboo_host/agentServer/agentInstaller/${bamboo_agent}
if [ $? -ne 0 ]; then
  exit_error "Failed to get bamboo client"
fi

#Create java keystore
rm -rf ${keystore_file}
keytool -noprompt -storepass ${password} -import -alias CernRootCa -file /etc/pki/tls/certs/CERN_Root_CA.crt -keystore ${keystore_file}
keytool -noprompt -storepass ${password} -import -alias BambooServerCert -file ${bamboo_cert_dir} -keystore ${keystore_file}
if [ $? -ne 0 ]; then
  exit_error "Failed to get create keystore"
fi

#Install capabilities
cat /etc/issue | grep Scientific &> /dev/null
if [ $? -eq 0 ]; then
  cat /etc/issue | grep "CERN" &> /dev/null
  if [ $? -eq 0 ]; then
    OS=SLC
  else
    OS=SL
  fi
else
  OS=`cat /etc/issue | head -n 1 | cut -f 1 -d " "`
fi

mkdir -p ${bamboo_home}/bin
cat > "${bamboo_home}/bin/bamboo-capabilities.properties" << EOF
system.arch=`uname -i`
system.operatingsystem=$OS
system.operatingsystemrelease=`cat /etc/issue | head -n 1 | sed 's|.*\([0-9]\.[0-9]\).*|\1|'`
system.osmajor=`cat /etc/issue | head -n 1 | sed 's|.*\([0-9]\)\.[0-9].*|\1|'`
system.osfamily=RedHat
EOF

#######################################################################
# Install and configure Bamboo client

java -Dbamboo.home=${bamboo_home} -Dbamboo.agent.ignoreServerCertName=true -Djavax.net.ssl.trustStore=${keystore_file} -Djavax.net.ssl.keyStorePassword=${password} -jar ${bamboo_agent} https://$bamboo_host/agentServer/ install &>bamboo-run.log
if [ $? -ne 0 ]; then
  exit_error "Failed to install client"
fi

set_file_prop ${wrapper_conf} "wrapper.java.additional.3" "-Djavax.net.ssl.trustStore=${keystore_file}"
set_file_prop ${wrapper_conf} "wrapper.java.additional.4" "-Djavax.net.ssl.keyStorePassword=${password}"

set_file_prop ${wrapper_conf} "wrapper.java.maxmemory" "1024"
set_file_prop ${wrapper_conf} "wrapper.java.initmemory" "512"
set_file_prop ${wrapper_conf} "wrapper.logfile.loglevel" "INFO"

set_file_prop ${wrapper_conf} "wrapper.max_failed_invocations"          10       #JVM restarts if it exists abnormally or is restarted shortly after launching
#set_file_prop ${wrapper_conf} "wrapper.successful_invocation_time"      300     #time to reset failed invocations counter
#set_file_prop ${wrapper_conf} "wrapper.startup.timeout"                 30      #time to check that it is running
#set_file_prop ${wrapper_conf} "wrapper.shutdown.timeout"                30      #time for jvm to shutdown if requested
#set_file_prop ${wrapper_conf} "wrapper.on_exit.default"                 RESTART #by default, restart if jvm exits
#set_file_prop ${wrapper_conf} "wrapper.on_exit.36"                      SHUTDOWN
set_file_prop ${wrapper_conf} "wrapper.restart.delay"                   120      #wait 30sec before restarting
#set_file_prop ${wrapper_conf} "wrapper.startup.delay"                   0       #first start delay

#######################################################################
#Run bamboo client

sh ${bamboo_home}/bin/bamboo-agent.sh start

log "Configuration successful"
exit 0
