#!/usr/bin/env python

import urllib2 as u2
import suds
from suds.transport.http import HttpTransport
import httplib
import subprocess
import socket
import os
import errno
import sys
import shutil
import re
from socket import gethostname
from email.MIMEText import MIMEText
import smtplib
import ConfigParser
import argparse

def parse_args():
    global hostname
    global password
    global ca_user_cert
    global username
    global target
    global email
    global verbose
    global openssl_config_file

    parser = argparse.ArgumentParser(description='Host certificate generator.')

    parser.add_argument('hostname', help='Hostname for which a certificate is issued')
    parser.add_argument('-p', "--password", action='store', default=None, \
        help="Password of the user certificate that is provided. If not defined, it will try to read 'password' environment variable")
    parser.add_argument('-c', '--certificate', required=True, action='store', help='Location of the user certificate. Example: /home/user/mycert.p12')
    parser.add_argument('-e', '--email', required=True, action='store', help='Email of the user. Example: my.email@cern.ch')
    parser.add_argument('-u', '--username', required=True, action='store', help='Username to whom the user certificate belongs to.')
    parser.add_argument('-t', '--target', action='store', default=".", help='Target directory, where generated host certificate and keys will be moved to. If not specified, they will stay in the current directory')
    parser.add_argument('-v', '--verbose', action='store_true', default=False, help="Verbose information")
    parser.add_argument('-o', '--openssl', action='store', help="OpenSSL config file path")

    ns = parser.parse_args()

    verbose = ns.verbose

    hostname = ns.hostname
    if not hostname.endswith("cern.ch"):
        hostname = hostname + ".cern.ch"

    target = ns.target
    if target is not None and not target.endswith("/"):
        target = target + "/"

    if ns.openssl:
        openssl_config_file = ns.openssl

    email = ns.email
    
    if ns.password:
        password = ns.password
    elif os.environ.get('password') is not None:
        password = os.environ['password']
    else:
        print "No password given. Please provide one as an argument or by setting the 'password' environment variable"
        sys.exit()

    if ns.certificate is not None:
        ca_user_cert = ns.certificate

    if ns.username is not None:
        username = ns.username

    print 'Requesting host certificate for %s, using user certificate %s of user %s' % (hostname, ca_user_cert, username)

def check_openssl_config(openssl_config_file):
    #openssl_config_file = '/usr/lib/ssl/openssl.cnf'
    #check if config file looks ok
    if os.path.isfile(openssl_config_file) and 'alt_names' in open(openssl_config_file).read():
        return 1

    #otherwise, append to it
    fopenssl = open(openssl_config_file, 'a')

    fopenssl.write('\n')
    fopenssl.write('[req]\n')
    fopenssl.write('req_extensions = v3_req\n')
    fopenssl.write('\n')
    fopenssl.write('[ v3_req ]\n')
    fopenssl.write('\n')
    fopenssl.write('basicConstraints = CA:FALSE\n')
    fopenssl.write('keyUsage = nonRepudiation, digitalSignature, keyEncipherment\n')
    fopenssl.write('subjectAltName = @alt_names\n')
    fopenssl.write('\n')
    fopenssl.write('[alt_names]\n')
    fopenssl.write('DNS.1 = %s\n' % hostname)
    fopenssl.write('\n')

    fopenssl.close()

    return 0

def silent_remove(filename):
    try:
        os.remove(filename)
    except OSError, e:
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occured

class HTTPSClientAuthHandler(u2.HTTPSHandler):
    def __init__(self, key, cert):
        u2.HTTPSHandler.__init__(self)
        self.key = key
        self.cert = cert

    def https_open(self, req):
        return self.do_open(self.getConnection, req)

    def getConnection(self, host, timeout=300):
        return httplib.HTTPSConnection(host, key_file=self.key, cert_file=self.cert)

class HTTPSClientCertTransport(HttpTransport):
    """
    Class to wrap suds request using client certificate & key
    """
    def __init__(self, key, cert, *args, **kwargs):
        HttpTransport.__init__(self, *args, **kwargs)
        self.key = key
        self.cert = cert

    def u2open(self, u2request):
        """
        Open a connection.
        @param u2request: A urllib2 request.
        @type u2request: urllib2.Requet.
        @return: The opened file-like urllib2 object.
        @rtype: fp
        """
        tm = self.options.timeout
        url = u2.build_opener(HTTPSClientAuthHandler(self.key, self.cert))
        if self.u2ver() < 2.6:
            socket.setdefaulttimeout(tm)
            return url.open(u2request)
        else:
            return url.open(u2request, timeout=tm)

############################################
#   VARIABLES
############################################
#location of CA certificates
CA_path = "/etc/grid-security/certificates"

# username and passwords
username = None
password = None

#output files
# openssl req -new -subj "/CN=host.cern.ch" -out tmp.csr -nodes -sha1 -newkey rsa:2048
request_file = "/tmp/tmp.csr"
host_key_file = "hostkey.pem"
host_cert_file = "hostcert.pem"
host_pks_file = "hostcert.pkcs12"
host_pass = "test"

target = None
verbose = None

# Mail address of the owner of the certificate
email = None
ca_user_cert = None

#files created from eticsadm's pksc12 certificate (eticsadm_cert_file)
# openssl pkcs12 -in certificate.pfx -out certificate.pem -nodes -clcerts
user_cert_file = "./certificate.pem"
user_key_file = "./certificate.pem"

# path to CA service
prod_url = "https://gridca.cern.ch/gridca-services/host.asmx?WSDL"

# Hostname to which we want to generate the certificate
hostname = None

openssl_config_file = '/usr/lib/ssl/openssl.cnf'

try:
    try:
        parse_args()
        check_openssl_config(openssl_config_file)

        ##
        # Create request certificate
        #
        command = "openssl req -new -subj \"/CN=" + hostname + "\" -out " + request_file + " -nodes -sha1 -newkey rsa:2048 -keyout " + host_key_file
        command_print = command
        if verbose:
            print 'COMMAND: ' + command_print
        sys.stdout.write('Creating certificate request for ' + hostname + ' in ' + request_file + '.. ')
        sys.stdout.flush()
        ret = subprocess.call(command, stdout=open('/dev/null', 'w'), stderr=subprocess.STDOUT, shell=True)
        if ret == 0:
            print 'Done'  
        else:
            print 'Failed'      

        ##
        # Read the request file contents 
        #
        request = open(request_file, 'r').read()

        ##
        # Generate user pem certificate / key
        #
        command = "openssl pkcs12 -in " + ca_user_cert + " -passin pass:" + password + " -out " + user_cert_file + " -nodes -clcerts"
        command_print = "openssl pkcs12 -in " + ca_user_cert + " -passin pass:***** -out " + user_cert_file + " -nodes -clcerts"
        if verbose:
            print 'COMMAND: ' + command_print
        sys.stdout.write('Creating user certificate/key pem.. ')
        sys.stdout.flush()
        ret = subprocess.call(command, stdout=open('/dev/null', 'w'), stderr=subprocess.STDOUT, shell=True)
        if ret == 0:
            print 'Done'
        else:
            print 'Failed'
 
        sys.stdout.write('Sending request for host certificate with user certificate/key.. ')
        sys.stdout.flush()
        
        ##
        # Create web service client
        #
        client = suds.client.Client(prod_url, transport=HTTPSClientCertTransport(user_key_file, user_cert_file))

        ##
        # Request the host certificate
        #
        result = client.service.NewHostCertificate(request, "None", email)

        if not result.Success:
            print "Failed:", result.Message
            print "ActivityID:", result.ActivityID
            print "RequestID:", result.RequestID
            raise Exception("WS call failed:" + result.Message)

        print "Done"
        fcert = open(host_cert_file, "w")
        fcert.write(result.Certificate)
        fcert.close()

        ##
        # Create pks certificate from pem, password protected
        #
        sys.stdout.write('Creating pks certificate from host cert and host key (password protected).. ')
        sys.stdout.flush()
        command = "openssl pkcs12 -export -in " + host_cert_file + " -inkey " + host_key_file + " -out " + host_pks_file + " -name \"computer\" -CAfile " + CA_path + "/CERN.chain  -chain -passout pass:" + host_pass
        ret = subprocess.call(command, stdout=open('/dev/null', 'w'), stderr=subprocess.STDOUT, shell=True)
        if ret == 0:
            print 'Done'
        else:
            print 'Failed!'

        ##
        # Copy certificates/keys to final directory
        #
        if target is not None: 
            shutil.move(host_cert_file, target + host_cert_file + "." + hostname)
            shutil.move(host_key_file, target + host_key_file + "." + hostname )
            shutil.move(host_pks_file, target + host_pks_file + "." + hostname )

    except Exception, e:
        print 'Error executing. Exiting'
        raise

finally:
    ##
    # Some housekeeping: remove temp files
    #
    silent_remove(request_file)
    silent_remove(user_cert_file)
    silent_remove(user_key_file)
    

