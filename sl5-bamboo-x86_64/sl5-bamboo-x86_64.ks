install
text
key --skip
keyboard us
lang en_US.UTF-8
skipx
network --bootproto=dhcp --device=eth0 --onboot=on
rootpw install
firewall --enabled  --ssh --port=7001:udp --port=4241:tcp
auth --useshadow --enablemd5 --disablenis
selinux --permissive
timezone --utc Europe/Zurich
bootloader --location=mbr --append="console=tty0 console=ttyS0,115200 loglevel=debug"
zerombr yes
clearpart --all --initlabel
#logging --host=137.138.102.220 --port=514 --level=debug

services --enabled=network

part / --size 10000 --fstype ext3   --ondisk vda --grow
part swap --size 4096

repo --name=CERNsl --baseurl=http://linuxsoft.cern.ch/scientific/59/x86_64/SL/
repo --name=CERNfastbugs --baseurl=http://linuxsoft.cern.ch/scientific/59/x86_64/updates/fastbugs/
repo --name=CERNsecurity --baseurl=http://linuxsoft.cern.ch/scientific/59/x86_64/updates/security/
repo --name=CERNdebuginfo --baseurl=http://linuxsoft.cern.ch/scientific/59/archive/debuginfo
repo --name=CERNepel  --baseurl=http://linuxsoft.cern.ch/epel/5/x86_64
repo --name=CERNepel-testing  --baseurl=http://linuxsoft.cern.ch/epel/testing/5/x86_64
repo --name=CERNai5 --baseurl=http://linuxsoft.cern.ch/koji/koji-x86_64/RPMS.ai5-os/

reboot

%packages --excludedocs
  @base
  @core
  cloud-init
  curl
  dhclient
  gcc
  git
  glibc
  grub
  hiera-puppet
  kernel
  java-1.6.0-openjdk
  mock
  ntp
  openssh-clients
  openssh-server
  pam_krb5
  puppet
  python
  python-devel
  python26-argparse
  #redhat-lsb
  rpmdevtools
  rpm-build
  rubygems
  rubygem-json
  rubygem-landb
  rubygem-savon
  sssd
  subversion
  tar
  tmux
  vim-enhanced
  virt-what
  yum
  yum-conf
  yum-utils
  yum-priorities
  yum-protectbase

%post --log=/root/post.log
echo "Post-install on"

#
# install custom cern repos and SL mirrors
#
rm -rf /etc/yum.repos.d/
mkdir /etc/yum.repos.d/
cd /etc/yum.repos.d/
REPOARCHIVE=sl5_repos.tar.gz
curl -s -L -O http://cern.ch/bamboofiles/${REPOARCHIVE}
tar -xf ${REPOARCHIVE}
rm -rf ${REPOARCHIVE}

#yum -y --enablerepo=slc5-updates install cern-get-keytab
yum -y --enablerepo=slc5-updates install CERN-CA-certs
#yum -y install cern-get-sso-cookie
#yum -y --enablerepo=slc5-updates install certmgr-client

yum clean all
yum update

#
# fix cloud init config
#
sed -i "s|^user: .*|user: root|" /etc/cloud/cloud.cfg
sed -i "s|^disable_root:.*|disable_root: 0|" /etc/cloud/cloud.cfg

#
# install afs
#
yum -y --disablerepo=sl install openafs-client openafs-krb5 krb5-workstation
rm -rf /etc/krb5.conf
wget http://linux.web.cern.ch/linux/docs/krb5.conf -O /etc/krb5.conf
mkdir -p /usr/vice/etc/
echo "cern.ch" > /usr/vice/etc/ThisCell
chkconfig afs on

#
# get boot messages to appear
#
for f in /boot/grub/grub.conf /boot/grub2/grub.conf; do
    if [ -f $f ]; then
        sed -i 's/ quiet//' $f
        sed -i 's/ rhgb//' $f
    fi
done

#
# other fixes
#
cd /tmp
git clone --depth 1 http://gitgw.cern.ch/git/ai-image-templates
cd ai-image-templates/scripts
./fixes

#
# Force first rotation of logs
#
/usr/sbin/logrotate -f /etc/logrotate.conf
%end

