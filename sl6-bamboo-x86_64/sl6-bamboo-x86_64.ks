install
text
key --skip
keyboard us
lang en_US.UTF-8
skipx
network --bootproto=dhcp --device=eth0 --onboot=on
rootpw install
firewall --enabled  --ssh --port=7001:udp --port=4241:tcp
auth --useshadow --enablemd5 --disablenis
selinux --permissive
timezone --utc Europe/Zurich
bootloader --location=mbr --timeout=1 --append="console=tty0 console=ttyS0,115200"
zerombr yes
clearpart --all --initlabel
#logging --host=137.138.102.220 --port=514 --level=debug

services --enabled=network

part / --size 10000 --fstype ext4   --ondisk vda --grow
part swap --size 4096

repo --name=CERNsl --cost 30 --baseurl=http://linuxsoft.cern.ch/scientific/6x/x86_64/os/
repo --name=CERNfastbugs --cost 30 --baseurl=http://linuxsoft.cern.ch/scientific/6x/x86_64/updates/fastbugs/
repo --name=CERNsecurity --cost 30 --baseurl=http://linuxsoft.cern.ch/scientific/6x/x86_64/updates/security/
repo --name=CERNdebuginfo --cost 30 --baseurl=http://linuxsoft.cern.ch/scientific/6x/archive/debuginfo
repo --name=CERNepel  --baseurl=http://linuxsoft.cern.ch/epel/6/x86_64
repo --name=CERNepel-testing  --baseurl=http://linuxsoft.cern.ch/epel/testing/6/x86_64
#repo --name=CERNai6 --cost 5 --baseurl=http://linuxsoft.cern.ch/koji/koji-x86_64/RPMS.ai6-os/

reboot

%packages --excludedocs
  @base
  @core
  #certmgr-client
  #cern-get-keytab
  #cern-get-sso-cookie
  #CERN-CA-certs
  cloud-init
  curl
  gcc
  git
  glibc
  grub
  #hiera-puppet
  java-1.6.0-openjdk
  kernel
  libselinux-python
  mock
  ntp
  openssh-clients
  pam_krb5
  policycoreutils-python
  puppet
  python
  python-argparse
  python-boto
  python-cheetah
  python-configobj
  python-devel
  python-prettytable
  PyYAML
  redhat-lsb
  rpmdevtools
  rpm-build
  #rubygems
  #rubygem-json
  #rubygem-landb
  #rubygem-savon
  system-config-firewall-base
  subversion
  system-config-firewall-base
  tar
  tmux
  vim
  yum-plugin-priorities

%end

%post --log=/root/post.log
echo "Post-install on"

#
# install custom cern repos and SL mirrors
#
rm -rf /etc/yum.repos.d/
mkdir /etc/yum.repos.d/
cd /etc/yum.repos.d/
REPOARCHIVE=sl6_repos.tar.gz
curl -s -L -O http://cern.ch/bamboofiles/${REPOARCHIVE}
tar -xf ${REPOARCHIVE}
rm -rf ${REPOARCHIVE}

#yum -y install cern-get-keytab
yum -y --enablerepo=slc-updates install CERN-CA-certs
#yum -y install cern-get-sso-cookie
#yum -y install certmgr-client

yum clean all
yum update

#
# fix cloud init config
#
sed -i "s|^user: .*|user: root|" /etc/cloud/cloud.cfg
sed -i "s|^disable_root:.*|disable_root: 0|" /etc/cloud/cloud.cfg

#
# install afs/kerberos
#
yum -y install openafs-client openafs-krb5 krb5-workstation
rm -rf /etc/krb5.conf
wget http://linux.web.cern.ch/linux/docs/krb5.conf -O /etc/krb5.conf
mkdir -p /usr/vice/etc/
echo "cern.ch" > /usr/vice/etc/ThisCell
chkconfig afs on

#
# get boot messages to appear
#
for f in /boot/grub/grub.conf /boot/grub2/grub.conf; do
    if [ -f $f ]; then
        sed -i 's/ quiet//' $f
        sed -i 's/ rhgb//' $f
    fi
done

#
# other fixes
#
cd /tmp
git clone --depth 1 http://gitgw.cern.ch/git/ai-image-templates
cd ai-image-templates/scripts
./fixes

#
# Force first rotation of logs
#
/usr/sbin/logrotate -f /etc/logrotate.conf
%end

